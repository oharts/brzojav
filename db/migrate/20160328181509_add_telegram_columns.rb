class AddTelegramColumns < ActiveRecord::Migration
  def change
    add_column :telegrams, :stornirano, :datetime
    add_column :telegrams, :stornirao, :string
    add_column :telegrams, :brojSuglasnosti, :integer
    add_column :telegrams, :korigirano, :datetime
    add_column :telegrams, :korigirao, :string
  end
end
