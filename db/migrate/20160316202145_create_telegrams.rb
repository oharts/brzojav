class CreateTelegrams < ActiveRecord::Migration
  def change
    create_table :telegrams do |t|
      t.string :salje
      t.string :prima
      t.string :poruka
      t.string :brojBrzojava
      t.string :vanjskiBroj
      t.string :sastavio

      t.timestamps null: false
    end
  end
end
