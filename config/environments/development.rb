class MyProjectMailLogger

  def self.delivering_email(message)
   if message.subject!="Dodjela broja brzojava"
   @to = message.to.to_s
   @message = message.html_part.body.to_s.force_encoding("utf-8").encode("utf-8")
   if @to=="[\"Borka.Kos@hops.hr\"]"
     @to="PrP Rijeka"
     @naslov="PrP Rijeka"
   end
   if message.subject=="SUGLASNOST MC RIJEKA - NISMO SUGLASNI"
   Telegram.create!(:primaNaslov => @naslov, :prima => @to, :salje => ENV['KORISNIK'], :sastavio => ENV['KORISNIK'], :poruka => @message, :odbijeno => "1", :brojSuglasnosti => ENV['SUGL'].to_i )
   ENV['SUGL']="x"
 elsif message.subject=="SUGLASNOST MC RIJEKA" or message.subject=="GREŠKA"
   Telegram.create!(:primaNaslov => @to, :prima => @to,  :salje => ENV['KORISNIK'], :sastavio => ENV['KORISNIK'],  :poruka => @message, :suglasnost => "1", :brojSuglasnosti => ENV['SUGL'].to_i )
   ENV['SUGL']="x"
 elsif ENV['VEZABR']!="0"
 Telegram.create!(:primaNaslov => @to, :prima => @to,  :salje => ENV['KORISNIK'], :sastavio => ENV['KORISNIK'],  :poruka => @message, :brojSuglasnosti => ENV['VEZABR'].to_i)
 ENV['VEZABR']="0"
 else
   Telegram.create!(:primaNaslov => @to, :prima => @to,  :salje => ENV['KORISNIK'], :sastavio => ENV['KORISNIK'],  :poruka => @message)
 end
 end
 end

end

Rails.application.configure do
  # Settings specified here will take precedence over those in config/application.rb.

  # In the development environment your application's code is reloaded on
  # every request. This slows down response time but is perfect for development
  # since you don't have to restart the web server when you make code changes.
  config.cache_classes = false

  # Do not eager load code on boot.
  config.eager_load = false

  # Show full error reports and disable caching.
  config.consider_all_requests_local       = true
  config.action_controller.perform_caching = false

  # Don't care if the mailer can't send.
  config.action_mailer.raise_delivery_errors = true
  config.action_mailer.perform_deliveries = true
  config.action_mailer.default_url_options = { :host => 'localhost:3000' }
  config.action_mailer.delivery_method = :smtp
  
  config.action_mailer.smtp_settings = {
      :address              => "mail.ho-pisup.com",
      :port                 => 26,
      :domain               => "ho-pisup.com",
      :user_name            => "dispeceri.rijeka@ho-pisup.com",
      :password             => "dispecer1",
      :authentication       => :plain,
      :enable_starttls_auto => false
}
ActionMailer::Base.register_interceptor(MyProjectMailLogger)

  # Print deprecation notices to the Rails logger.
  config.active_support.deprecation = :log

  # Raise an error on page load if there are pending migrations.
  config.active_record.migration_error = :page_load

  # Debug mode disables concatenation and preprocessing of assets.
  # This option may cause significant delays in view rendering with a large
  # number of complex assets.
  config.assets.debug = true

  # Asset digests allow you to set far-future HTTP expiration dates on all assets,
  # yet still be able to expire them through the digest params.
  config.assets.digest = true

  # Adds additional error checking when serving assets at runtime.
  # Checks for improperly declared sprockets dependencies.
  # Raises helpful error messages.
  config.assets.raise_runtime_errors = true

  # Raises error for missing translations
  # config.action_view.raise_on_missing_translations = true
end
