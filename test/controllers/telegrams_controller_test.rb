require 'test_helper'

class TelegramsControllerTest < ActionController::TestCase
  setup do
    @telegram = telegrams(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:telegrams)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create telegram" do
    assert_difference('Telegram.count') do
      post :create, telegram: { brojBrzojava: @telegram.brojBrzojava, poruka: @telegram.poruka, prima: @telegram.prima, salje: @telegram.salje, sastavio: @telegram.sastavio, vanjskiBroj: @telegram.vanjskiBroj }
    end

    assert_redirected_to telegram_path(assigns(:telegram))
  end

  test "should show telegram" do
    get :show, id: @telegram
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @telegram
    assert_response :success
  end

  test "should update telegram" do
    patch :update, id: @telegram, telegram: { brojBrzojava: @telegram.brojBrzojava, poruka: @telegram.poruka, prima: @telegram.prima, salje: @telegram.salje, sastavio: @telegram.sastavio, vanjskiBroj: @telegram.vanjskiBroj }
    assert_redirected_to telegram_path(assigns(:telegram))
  end

  test "should destroy telegram" do
    assert_difference('Telegram.count', -1) do
      delete :destroy, id: @telegram
    end

    assert_redirected_to telegrams_path
  end
end
