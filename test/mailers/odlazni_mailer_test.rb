require 'test_helper'

class OdlazniMailerTest < ActionMailer::TestCase
  test "broj_mailer" do
    mail = OdlazniMailer.broj_mailer
    assert_equal "Broj mailer", mail.subject
    assert_equal ["to@example.org"], mail.to
    assert_equal ["from@example.com"], mail.from
    assert_match "Hi", mail.body.encoded
  end

  test "suglasnost_mailer" do
    mail = OdlazniMailer.suglasnost_mailer
    assert_equal "Suglasnost mailer", mail.subject
    assert_equal ["to@example.org"], mail.to
    assert_equal ["from@example.com"], mail.from
    assert_match "Hi", mail.body.encoded
  end

  test "odbijeno_mailer" do
    mail = OdlazniMailer.odbijeno_mailer
    assert_equal "Odbijeno mailer", mail.subject
    assert_equal ["to@example.org"], mail.to
    assert_equal ["from@example.com"], mail.from
    assert_match "Hi", mail.body.encoded
  end

  test "storno_mailer" do
    mail = OdlazniMailer.storno_mailer
    assert_equal "Storno mailer", mail.subject
    assert_equal ["to@example.org"], mail.to
    assert_equal ["from@example.com"], mail.from
    assert_match "Hi", mail.body.encoded
  end

  test "opci_mailer" do
    mail = OdlazniMailer.opci_mailer
    assert_equal "Opci mailer", mail.subject
    assert_equal ["to@example.org"], mail.to
    assert_equal ["from@example.com"], mail.from
    assert_match "Hi", mail.body.encoded
  end

end
