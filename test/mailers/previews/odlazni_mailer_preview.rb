# Preview all emails at http://localhost:3000/rails/mailers/odlazni_mailer
class OdlazniMailerPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/odlazni_mailer/broj_mailer
  def broj_mailer
    broj_brzojava=Telegram.last.brojBrzojava
    OdlazniMailer.broj_mailer(broj_brzojava)
  end

  # Preview this email at http://localhost:3000/rails/mailers/odlazni_mailer/suglasnost_mailer
  def suglasnost_mailer
    OdlazniMailer.suglasnost_mailer
  end

  # Preview this email at http://localhost:3000/rails/mailers/odlazni_mailer/odbijeno_mailer
  def odbijeno_mailer
    OdlazniMailer.odbijeno_mailer
  end

  # Preview this email at http://localhost:3000/rails/mailers/odlazni_mailer/storno_mailer
  def storno_mailer
    OdlazniMailer.storno_mailer
  end

  # Preview this email at http://localhost:3000/rails/mailers/odlazni_mailer/opci_mailer
  def opci_mailer
    OdlazniMailer.opci_mailer
  end

end
