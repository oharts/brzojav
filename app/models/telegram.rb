class Telegram < ActiveRecord::Base
    before_create :broj
    after_save :stornokorekcija
    after_save :setsuglasnost
    
    
    
    
    def broj
        self.brojBrzojava=Telegram.last.brojBrzojava.to_i+1
        
        if self.salje=="Borka.Kos@hops.hr"
            OdlazniMailer.broj_mailer(self.brojBrzojava, self.vanjskiBroj).deliver_now
        end
        
        if self.prima=="[\"dcpula@ho-pisup.com\"]"
               self.primaNaslov="DC ELEKTROISTRA"
        end
           
            
            
            
    end
    
    def stornokorekcija
      
            if self.storno=="1"
                Telegram.where(brojSuglasnosti: self.brojBrzojava).update_all(storno: "1", stornirao: self.stornirao, stornirano: self.stornirano)
            end
            if self.korigirano!=nil
                Telegram.where(brojSuglasnosti: self.brojBrzojava).update_all(korigirao: self.korigirao, korigirano: self.korigirano)
            end
            if self.odbijeno=="1"
                Telegram.where(brojSuglasnosti: self.brojBrzojava).update_all(odbijeno: "1")
            end
             
        
    end
    
    def setsuglasnost
            if self.suglasnost=="1"
                Telegram.where(brojSuglasnosti: self.brojBrzojava).update_all(suglasnost: "1")
            end
    end
    
    
    
end
