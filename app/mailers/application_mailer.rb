class ApplicationMailer < ActionMailer::Base
  default from: "Dispečerski centar Rijeka <dispeceri.rijeka@ho-pisup.com>"
  layout 'mailer'
end
