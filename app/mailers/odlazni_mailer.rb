class OdlazniMailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.odlazni_mailer.broj_mailer.subject
  #
  def broj_mailer(broj_brzojava, vanjski_broj)
    @broj_brzojava = broj_brzojava
    @vanjski_broj=vanjski_broj
    
  # Email od Borke za dodjelu broja
    mail to: "Borka.Kos@hops.hr",
         subject: "Dodjela broja brzojava"  
  end

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.odlazni_mailer.suglasnost_mailer.subject
  #
  def suglasnost_mailer(ime, prezime, broj_brzojava, vanjski_broj, sadrzaj)
    @broj_brzojava = broj_brzojava
    @vanjski_broj=vanjski_broj
    @sadrzaj=sadrzaj
    @ime=ime
    @prezime=prezime

    mail to: "Borka.Kos@hops.hr",
        subject: "SUGLASNOST MC RIJEKA"  
  end

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.odlazni_mailer.odbijeno_mailer.subject
  #
  def odbijeno_mailer(ime, prezime, za, broj_brzojava, vanjski_broj, sadrzaj)
    @broj_brzojava = broj_brzojava
    @vanjski_broj=vanjski_broj
    @sadrzaj=sadrzaj
    @za=za
    @ime=ime
    @prezime=prezime
    @razlog=ENV['RAZLOG']
    mail to: @za,
        subject: "SUGLASNOST MC RIJEKA - NISMO SUGLASNI" 
  end

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.odlazni_mailer.storno_mailer.subject
  #
  def storno_mailer
    @greeting = "Hi"

    mail to: "to@example.org"
  end

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.odlazni_mailer.opci_mailer.subject
  #
  def opci_mailer(ime, prezime, broj_brzojava, sadrzaj, salje)
    @broj_brzojava = broj_brzojava
    @sadrzaj=sadrzaj
    @salje=salje
    @ime=ime
    @prezime=prezime
  

    mail to: @salje,
      subject: "Suglasnost MC Rijeka"
  
  end
  
  def posaljina_mailer(ime, prezime, broj_brzojava, poruka, za)
    @broj_brzojava = broj_brzojava
    @poruka=poruka
    @ime=ime
    @prezime=prezime
    @za=za
    mail to: @za,
      subject: "Brzojav broj #{@broj_brzojava}"
    
  end
  
end
