class TelegramsController < ApplicationController
  before_action :set_telegram, only: [:show, :edit, :update, :destroy]
before_filter :set_search
  # GET /telegrams
  # GET /telegrams.json
  def index
    if params[:q].blank?
      
    @q = Telegram.all.search
    else
   @q = Telegram.ransack(params[:q])
    end
    
    
    @telegrams = @q.result.paginate(page: params[:page], :per_page => 10).order('brojBrzojava DESC')
    ENV['KORISNIK']=current_user.ime+" "+current_user.prezime
    @@tele=@q.result
  end

  # GET /telegrams/1
  # GET /telegrams/1.json
  def show
  end

  # GET /telegrams/new
  def new
    @telegram = Telegram.new
  end

  # GET /telegrams/1/edit
  def edit
  end

  # POST /telegrams
  # POST /telegrams.json
  def create
    @telegram = Telegram.new(telegram_params)

    respond_to do |format|
      if @telegram.save
        format.html { redirect_to @telegram, notice: 'Brzojav uspješno obrađen.' }
        format.json { render :show, status: :created, location: @telegram }
      else
        format.html { render :new }
        format.json { render json: @telegram.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /telegrams/1
  # PATCH/PUT /telegrams/1.json
  def update
    respond_to do |format|
      if @telegram.update(telegram_params)
        format.html { redirect_to telegrams_path, notice: 'Brzojav uspješno obrađen.' }
        format.json { render :show, status: :ok, location: @telegram }
        format.js {}
      else
        format.html { render :edit }
        format.json { render json: @telegram.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /telegrams/1
  # DELETE /telegrams/1.json
  def destroy
    @telegram.destroy
    respond_to do |format|
      format.html { redirect_to telegrams_url, notice: 'Telegram was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
  
  def suglasnost
      @telegram_stari = Telegram.find(params[:id])
      ime=current_user.ime
       prezime=current_user.prezime
      Telegram.find(params[:id]).update_attributes(:suglasnost => "1", :brojSuglasnosti => Telegram.last.brojBrzojava.to_i+1)
      ENV['SUGL']=@telegram_stari.brojBrzojava.to_s
      OdlazniMailer.suglasnost_mailer(ime, prezime, Telegram.last.brojBrzojava.to_i+1, @telegram_stari.vanjskiBroj, @telegram_stari.poruka).deliver_now
      flash[:notice] = "Poslana suglasnost."
      
      redirect_to telegrams_path
  end
  
    def suglasnostopca
      @telegram_stari = Telegram.find(params[:id])
       por="SUGLASNI s isklopom"+@telegram_stari.poruka
       ime=current_user.ime
       prezime=current_user.prezime
      Telegram.find(params[:id]).update_attributes(:suglasnost => "1", :brojSuglasnosti => Telegram.last.brojBrzojava.to_i+1)
      ENV['SUGL']=@telegram_stari.brojBrzojava.to_s
      if @telegram_stari.salje.include? "@" 
      OdlazniMailer.opci_mailer(ime, prezime, Telegram.last.brojBrzojava.to_i+1,  @telegram_stari.poruka, @telegram_stari.salje).deliver_now
      else
     Telegram.create!(:brojSuglasnosti => @telegram_stari.brojBrzojava, :salje => ENV['KORISNIK'], :sastavio => ENV['KORISNIK'], :prima => @telegram_stari.salje, :primaNaslov => @telegram_stari.salje, :poruka => por, :suglasnost => "1")
  end
      flash[:notice] = "Poslana suglasnost."
      
      redirect_to telegrams_path
  end
  
  def odbijeno
    @telegram_stari = Telegram.find(params[:id])
   ime=current_user.ime
       prezime=current_user.prezime
 
    Telegram.find(params[:id]).update_attributes(:odbijeno => "1")
    ENV['SUGL']=@telegram_stari.brojBrzojava.to_s
    ENV['RAZLOG']=params[:razlog]
     por="NISMO SUGLASNI "+ENV['RAZLOG']+@telegram_stari.poruka
    if @telegram_stari.salje.include? "@" 
    OdlazniMailer.odbijeno_mailer(ime, prezime, @telegram_stari.salje , Telegram.last.brojBrzojava.to_i+1, @telegram_stari.vanjskiBroj, @telegram_stari.poruka).deliver_now
  else
     Telegram.create!(:brojSuglasnosti => @telegram_stari.brojBrzojava, :salje => ENV['KORISNIK'], :sastavio => ENV['KORISNIK'], :prima => @telegram_stari.salje, :primaNaslov => @telegram_stari.salje, :poruka => por, :odbijeno => "1")
  end
    ENV['RAZLOG']="0"
    flash[:notice] = "Brzojav odbijen."
    redirect_to telegrams_path
  end
  
  def posaljina
      ime=current_user.ime
       prezime=current_user.prezime
    @telegram_stari = Telegram.find(params[:id])
    if params[:telegram].present?
    por=params[:telegram][:poruka]
  else
    por=params[:telegram2][:poruka]
  end
    if params[:ozn]!="n"
    ENV['VEZABR']=@telegram_stari.brojBrzojava.to_s
  else
    ENV['VEZABR']="0"
  end
    if params[:za].include? "@" 
    OdlazniMailer.posaljina_mailer(ime, prezime, Telegram.last.brojBrzojava.to_i+1, por, params[:za]).deliver_now
  else
    if params[:osoba].present?
      osoba=params[:osoba]
    else
      naslov="-"
    end
    Telegram.create!(:salje => ENV['KORISNIK'], :sastavio => ENV['KORISNIK'], :prima => osoba, :primaNaslov => params[:za], :poruka => por, :brojSuglasnosti => ENV['VEZABR'].to_i)
    ENV['VEZABR']="0"
    flash[:notice] = "Brzojav je poslan."
  end
    redirect_to telegrams_path
  end
  
  
  def search
   
  end
  
  def printanje
    
    @telegrams=@@tele
  end
  
  def primi
    por=params[:telegram8][:poruka]
    osoba=params[:osoba]
    organizacija=params[:organizacija]
    Telegram.create!(:prima => ENV['KORISNIK'], :sastavio => organizacija+" - "+osoba, :salje => osoba, :primaNaslov => "MC RIJEKA", :poruka => por)
    flash[:notice] = "Brzojav je primljen."
    redirect_to telegrams_path
  end
  

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_telegram
      @telegram = Telegram.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def telegram_params
      params.require(:telegram).permit(:primaNaslov, :salje, :prima, :poruka, :brojBrzojava, :vanjskiBroj, :sastavio, :suglasnost, :storno, :stornirano, :stornirao, :brojSuglasnosti, :korigirano, :korigirao)
    end
end
