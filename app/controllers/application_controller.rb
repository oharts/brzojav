class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  before_filter :configure_permitted_parameters, if: :devise_controller?
  protect_from_forgery with: :exception
  def after_sign_in_path_for(user)
    telegrams_path
   end
  def after_sign_out_path_for(user)
    login_path
  end
  def configure_permitted_parameters
  devise_parameter_sanitizer.for(:sign_up) { |u| u.permit(:username,:ime, :prezime,  :email, :password, :password_confirmation) }
end
def set_search
@q=Telegram.search(params[:q])
end
  
end
