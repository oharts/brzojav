json.array!(@telegrams) do |telegram|
  json.extract! telegram, :id, :salje, :prima, :poruka, :brojBrzojava, :vanjskiBroj, :sastavio
  json.url telegram_url(telegram, format: :json)
end
