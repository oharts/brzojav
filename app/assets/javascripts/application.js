// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery.turbolinks
//= require bootstrap-sprockets
//= require jquery_ujs
//= require ckeditor-jquery
//= require jquery-ui
//= require best_in_place
//= require best_in_place.jquery-ui
//= require bootstrap-datepicker
//= require_tree .

$(document).ready(function() {

});


$(document).ready(function() {
  $('[data-toggle="tooltip"]').tooltip();
});


$(document).on('ready page:load', function () {
  /* Activating Best In Place */
  jQuery(".best_in_place").best_in_place();


});

$(document).ready(function() {


	$('.input-daterange').datepicker({
		 format: "dd/mm/yyyy",
    todayBtn: "linked",
    language: "hr",
    todayHighlight: true
	});
});