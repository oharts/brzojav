CKEDITOR.editorConfig = (config) ->

  config.width = '40%'
  config.uiColor = "#AADC6E"
  config.colorButton_colors = '00923E,ff0000,000000';
  config.colorButton_enableMore = false;
  config.toolbar = [
       [ 'TextColor', 'Strike', 'Bold', 'Italic' ]
];
 
  true
